#include "Events.h"
#include <cstring>
#include <iostream>


// Constructor: Allocates Memory For Data Members
event::event()
{
	name = nullptr;
	description = nullptr;
	startDate = nullptr;
	endDate = nullptr;
	price = 0.0;
	userReview = nullptr;
	freeParkingCheck = nullptr;


}

// Destructor: Deallocates Memory For Data Members To Prevent Memory Leaks
event::~event()
{
	delete[] name;
	delete[] description;
	delete[] startDate;
	delete[] endDate;
	delete[] userReview;
	delete[] freeParkingCheck;
}
void event::setName(const char* newName)
{
	delete[] name;
	name = new char[strlen(newName) + 1];
	strcpy(name, newName);
};
void event::setDescription(const char* newDescription)
{
	delete[] description;
	description = new char[strlen(newDescription) + 1];
	strcpy(description, newDescription);
}
void event::setStartDate(const char* newStartDate)
{
	delete[] startDate;
	startDate = new char[strlen(newStartDate) + 1];
	strcpy(startDate, newStartDate);
}
void event::setEndDate(const char* newEndDate)
{
	delete[] endDate;
	endDate = new char[strlen(newEndDate) + 1];
	strcpy(endDate, newEndDate);
}
void event::setUserReview(const char* newUserReview)
{
	delete[] userReview;
	userReview = new char[strlen(newUserReview) + 1];
	strcpy(userReview, newUserReview);
}
void event::setFreeParkingCheck(const char* newFreeParkingCheck)
{
	delete[] freeParkingCheck;
	freeParkingCheck = new char[strlen(newFreeParkingCheck) + 1];
	strcpy(freeParkingCheck, newFreeParkingCheck);	
}




const char* event::getName() const
{
	return name;
}

const char* event::getDescription() const
{
	return description;
}

const char* event::getStartDate() const
{
	return startDate;
}

const char* event::getEndDate() const
{
	return endDate;
}

const char* event::getUserReview() const 
{
	return userReview;
}

const char* event::getFreeParkingCheck() const
{
	return freeParkingCheck;
}


void event::input() 
{
	char tempName[100];
	char tempDescription[500];
	char tempStartDate[100];
	char tempEndDate[100];
	char tempUserReview[500];
	char tempFreeParkingCheck[100];

	cout << "Enter Event Name: ";
	cin.getline(tempName, sizeof(tempName));
	setName(tempName);


	cout << "Enter Event Description: ";
	cin.getline(tempDescription, sizeof(tempDescription));
	setDescription(tempDescription);

	cout << "Enter Start Date: ";
	cin.getline(tempStartDate, sizeof(tempStartDate));
	setStartDate(tempStartDate);

	cout << "Enter End Date: ";
	cin.getline(tempEndDate, sizeof(tempEndDate));
	setEndDate(tempEndDate);

	cout << "Enter The User Review Score Of Event: ";
	cin.getline(tempUserReview, sizeof(tempUserReview));
	setUserReview(tempUserReview);

	cout << "Enter If Event Has Free Parking: ";
	cin.getline(tempFreeParkingCheck, sizeof(tempFreeParkingCheck));
	setFreeParkingCheck(tempFreeParkingCheck);

	cout << "Enter Event Price: ";
	cin >> price;
	cin.ignore();
}

void event::display() const
{
	cout << "Event Name: " << getName() << endl;
	cout << "Event Description: "<< getDescription() << endl;
	cout << "Event Start Date: " << getStartDate() << endl;
	cout << "Event End Date: " << getEndDate() << endl;
	cout << "Event User Review Score: " << getUserReview() << endl;
	cout << "Event Free Parking: " << getFreeParkingCheck() << endl;
	cout << "Event Price: " << price << endl;
}

int event::getPrice()
{
	return price;
}




