#include "Events.h"
#include <iostream>


int main()
{
	int numEvents;
	char searchCheck = 'n';
	int priceFilter;

	cout << "Enter Number Of Events: "  << endl;
	cin >> numEvents;
	cin.ignore();

	event* events = new event[numEvents];

	for (int i = 0; i < numEvents; ++i)
	{
		cout << "Event " << i + 1 << ":" << endl;
		events[i].input();
	}
	

	cout << "\nAll Entered Events:\n";
	for (int i = 0; i < numEvents; ++i)
	{
		cout << "Event " << i + 1 << ":" << endl;
		events[i].display();
		cout << endl;
	}
	cout << "would you like to look at events under a price point?: " << endl;
	cin >> searchCheck;
	cin.ignore();

	if (searchCheck == 'y')
	{
		cout << "Please Enter Price Point: " << endl;
		cin >> priceFilter;
		cin.ignore();
		cout << "Events Under " << priceFilter << endl;
		for (int i = 0; i < numEvents; ++i)
		{
			if(events[i].getPrice() <= priceFilter)
			{
				events[i].display();
			}
		}
	}




	delete[] events;

	return 0;



}
