#ifndef EVENTS_H_
#define EVENTS_H_
using namespace std;


class event
{
	public:
		event();
		~event();
		void setName(const char* name);
		void setDescription(const char* description);
		void setStartDate(const char* startDate);
		void setEndDate(const char* endDate);
		void setUserReview(const char* userReview);
		void setFreeParkingCheck(const char* freeParkingCheck);
		const char* getName() const;
		const char* getDescription() const;
		const char* getStartDate() const;
		const char* getEndDate() const;
		const char* getUserReview() const;
		const char* getFreeParkingCheck() const;
		void input();
		void display() const;
		int getPrice();





	private:
		char* name;
		char* description;
		char* startDate;
		char* endDate;
		float price;
		char* userReview;
		int ageRequirement;
		char* freeParkingCheck;
		const int MAX_LENGTH = 256;
		int numberOfEvents;

		

};


#endif
